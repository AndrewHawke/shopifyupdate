﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using ShopifyUpDATE.Models;
using ShopifySharp;

namespace ShopifyUpDATE
{


    class Program
    {
        public static IConfigurationRoot _config { get; set; }

        static PimDBContext _context { get; set; }
        static public DbContextOptionsBuilder<PimDBContext> _optionsBuilder { get; set; }

        static List<PimTag> _tags;
        static List<PimProductType> _productTypes;

        static ProductService _pService;



        static private async void LoopProducts()
        {

            var products = _context.PimProducts.AsNoTracking();

            foreach (var prod in products)
            {

                if (prod.B2cproductId == null) continue;

                string tags = "";
                var pTags = _context.PimProductTags.AsNoTracking().Where(x => x.PimProductId == prod.Id);
                foreach (var ptag in pTags)
                {
                    var tt = _tags.FirstOrDefault(x => x.Id == ptag.PimTagId);
                    if (tt != null)
                        tags = tags + tt.Name + ",";
                }

                var v = _context.PimProductVariants.AsNoTracking().Where(x => x.PimProductId == prod.Id).Select(z => new { z.EscDollarsCustomer }).First();

                if (v.EscDollarsCustomer != null)
                {

                    if (tags != "")
                    {
                        tags = tags + "Esc$_" + (Decimal.ToInt32((decimal)v.EscDollarsCustomer) * 100).ToString();
                    }

                    else
                    {
                        tags = "Esc$_" + (Decimal.ToInt32((decimal)v.EscDollarsCustomer) * 100).ToString();
                    }
                }
                else
                {
                    tags = tags.Substring(0, tags.Length - 1);  // remove last comma
                }

                if (!string.IsNullOrEmpty(prod.DealerDiscipline))
                {
                    tags = tags + "," + "twoesc-dealer_discipline_" + prod.DealerDiscipline;
                }

                var pType = _productTypes.FirstOrDefault(x => x.Name == prod.ProductType);
                if (!pType.RedeemableUsingEscDollars)
                    tags = tags + "," + "na_E$";

                Console.WriteLine(prod.Brand + " " + prod.Title + " " + prod.ModelYear + " ... New Tags:" + tags);

                var product = await _pService.GetAsync((long)prod.B2cproductId);

                if (product != null)
                {
                    Console.WriteLine("... old Tags: " + product.Tags);

                    product.Tags = tags;

   //                 var aproduct = await _pService.UpdateAsync((long)product.Id, new Product()
   //                 {
   //                     Tags = product.Tags
   //                 });

                    Console.WriteLine("... Updated");

                }
                else
                {
                    Console.WriteLine(" ... not found");
                }
            }


        }

        [STAThread]
        static void Main()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            //   .AddEnvironmentVariables();


            _config = builder.Build();
            Console.WriteLine($"Website = {_config["Shopify:CustomerWebsite"]}");
            Console.WriteLine($"Password = {_config["Shopify:CustomerPassword"]}");

            _pService = new ProductService(_config["Shopify:CustomerWebsite"], _config["Shopify:CustomerPassword"]);


            var sqlstr = _config["ConnectionStrings:PimDBConnection"];

            _optionsBuilder = new DbContextOptionsBuilder<PimDBContext>();
            _optionsBuilder.UseSqlServer(sqlstr);

            _context = new PimDBContext(_optionsBuilder.Options);

            _tags = _context.PimTags.AsNoTracking().ToList();
            _productTypes = _context.PimProductTypes.AsNoTracking().ToList();

            LoopProducts();

            Console.WriteLine("All complete");

            Console.ReadLine();


        }
    }
}
