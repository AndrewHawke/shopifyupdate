﻿using System;
using System.Collections.Generic;

namespace ShopifyUpDATE.Models
{
    public partial class PimSystemColour
    {
        public int Id { get; set; }
        public string Colour { get; set; }
    }
}
