﻿using System;
using System.Collections.Generic;

namespace ShopifyUpDATE.Models
{

    // this class used when importing products from PIM to help make the Variant to Image Id link

    public partial class PimVarSkuImageLink
    {
        public string Sku { get; set; }
        public int PimImageId { get; set; }
    }
    public partial class PimProductVariant
    {
        public int Id { get; set; }
        public int? PimProductId { get; set; }
        public int? ProductImageId { get; set; }
        public int? PimBrandColourId { get; set; }
        public string Sku { get; set; }
        public string Upc { get; set; }
        public string Mpn { get; set; }
        public string Title { get; set; }
        public decimal? Weight { get; set; }
        public string WeightUnit { get; set; }
        public string Option1 { get; set; }
        public string Option2 { get; set; }
        public string Option3 { get; set; }
        public int? Position { get; set; }
        public bool? RedeemableUsingEscDollars { get; set; }
        public bool? SaleOnB2b { get; set; }
        public int? MoqDealerA { get; set; }
        public int? MoqDealerB { get; set; }
        public int? MoqDealerC { get; set; }
        public string ForeignCurrencyUnits { get; set; }
        public decimal? CostOfGoodsFc { get; set; }
        public decimal? XrateBuying { get; set; }
        public decimal? CostOfGoods { get; set; }
        public decimal? ShippingAndDuties { get; set; }
        public decimal? LandedCost { get; set; }
        public decimal? MsrpFc { get; set; }
        public decimal? XrateSelling { get; set; }
        public decimal? Msrp { get; set; }
        public decimal? EscapeSrp { get; set; }
        public decimal? EscapeSrpWithGst { get; set; }
        public decimal? EscapeSrpDisc { get; set; }
        public decimal? EscapeSrpDiscWithGst { get; set; }
        public decimal? EscDollarsCustomer { get; set; }
        public decimal? RetailEscDollars { get; set; }
        public decimal? RetailEscDollarsWithDisc { get; set; }
        public decimal? CommissionDealerA { get; set; }
        public decimal? CommissionDealerB { get; set; }
        public decimal? CommissionDealerC { get; set; }
        public decimal? DealerASalePrice { get; set; }
        public decimal? DealerASalePriceWithGst { get; set; }
        public decimal? EscDollarsDealerA { get; set; }
        public decimal? DealerBSalePrice { get; set; }
        public decimal? DealerBSalePriceWithGst { get; set; }
        public decimal? EscDollarsDealerB { get; set; }
        public decimal? DealerCSalePrice { get; set; }
        public decimal? DealerCSalePriceWithGst { get; set; }
        public decimal? EscDollarsDealerC { get; set; }
        public string ShopifyInventoryManagement { get; set; }
        public string ShopifyInventoryPolicy { get; set; }
        public bool? ShopifyRequiresShipping { get; set; }
        public bool? ShopifyTaxable { get; set; }
        public string ShopifyFulfillmentService { get; set; }
        public long? B2cvariantId { get; set; }
        public long? B2bvariantId { get; set; }
        public long? WarehouseVariantId { get; set; }
        public bool? IsDirty { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public virtual PimBrandColour PimBrandColour { get; set; }
        public virtual PimProduct PimProduct { get; set; }
        public virtual PimProductImage ProductImage { get; set; }
    }
}
