﻿using System;
using System.Collections.Generic;

namespace ShopifyUpDATE.Models
{
    public partial class PimProductType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool RedeemableUsingEscDollars { get; set; }
        public string OptionsCsv { get; set; }
        public int? VolumetricWeight { get; set; }
        public bool? DealerCyes { get; set; }
        public string TagColour { get; set; }
        public string TagMaterial { get; set; }
        public string TagGender { get; set; }
        public string TagDiscipline { get; set; }
        public string TagDealerDiscipline { get; set; }
        public string VariantBrandColour { get; set; }
        public string VariantSize { get; set; }
        public string VariantLength { get; set; }
        public string VariantRise { get; set; }
        public string VariantTravel { get; set; }
        public string VariantType { get; set; }
    }
}
