﻿
using Microsoft.EntityFrameworkCore;
using ShopifyUpDATE.Models;
using Microsoft.EntityFrameworkCore.Metadata;


/* This context class is maintained by hand
 * the project is a database first project
 * In order to make changes to the model use the following command from the Packae manager console
 * 
 *  From command line of the project
 *  dotnet ef dbcontext scaffold "Server=.;Database=Insero;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -o Models -f
 *  
 * This recreates the model classes ( except PimUser ).
 * IT also creates a context class "InseroContext". Use the code required from the InseroContext class to onModelCreating function in this class.
 * 
 *  */
namespace ShopifyUpDATE.Models
{
    public class PimDBContext : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=2escapesqlsrvr.database.windows.net,1433;Initial Catalog=2EscapePIMDB;Persist Security Info=False;User ID='sqladmin';Password='!#23scap3$%';MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }


        public PimDBContext(DbContextOptions<PimDBContext> options)
            : base(options)
        {
        }

        public DbSet<PimBrand> PimBrands { get; set; }
        public DbSet<PimBrandColour> PimBrandColours { get; set; }
        public DbSet<PimCustomer> PimCustomers { get; set; }

        public DbSet<PimProduct> PimProducts { get; set; }
        public DbSet<PimProductImage> PimProductImages { get; set; }

        public DbSet<PimProductTag> PimProductTags { get; set; }
        public DbSet<PimProductType> PimProductTypes { get; set; }


        public DbSet<PimProductVariant> PimProductVariants { get; set; }

        public DbSet<PimSystemColour> PimSystemColours { get; set; } 
        public DbSet<PimTag> PimTags { get; set; }

        public DbSet<PimProductNote> PimProductNotes { get; set; }





        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            /* Beginning of Direct Copy from  Insero Context class do not include AspNetTables */

            modelBuilder.Entity<PimBrand>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("IX_PimBrand_Name")
                    .IsUnique();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<PimBrandColour>(entity =>
            {
                entity.Property(e => e.BrandColour).IsRequired();

                entity.Property(e => e.SystemColoursCsv).IsRequired();

                entity.HasOne(d => d.PimBrand)
                    .WithMany(p => p.PimBrandColour)
                    .HasForeignKey(d => d.PimBrandId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_PimBrandColour_PimBrand");
            });

            modelBuilder.Entity<PimCustomer>(entity =>
            {
                entity.Property(e => e.Currency).HasMaxLength(50);

                entity.Property(e => e.DatabaseName).HasMaxLength(200);

                entity.Property(e => e.Domain).HasMaxLength(200);

                entity.Property(e => e.Name).HasMaxLength(400);

                entity.Property(e => e.SkuAbbreviation).HasMaxLength(10);
            });

            modelBuilder.Entity<PimProduct>(entity =>
            {
                entity.Property(e => e.B2bproductId).HasColumnName("B2BProductId");

                entity.Property(e => e.B2bupdatedAt)
                    .HasColumnName("B2BUpdatedAt")
                    .HasColumnType("datetime");

                entity.Property(e => e.B2cproductId).HasColumnName("B2CProductId");

                entity.Property(e => e.B2cupdatedAt)
                    .HasColumnName("B2CUpdatedAt")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.DealerDiscipline).HasMaxLength(200);

                entity.Property(e => e.Handle).HasColumnName("handle");

                entity.Property(e => e.ProductType).HasMaxLength(200);

                entity.Property(e => e.PublishedAt).HasColumnType("datetime");

                entity.Property(e => e.Title).IsRequired();

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.WarehouseStatus).HasMaxLength(200);

                entity.Property(e => e.WarehouseUpdatedAt).HasColumnType("datetime");

                entity.HasOne(d => d.PimCustomer)
                    .WithMany(p => p.PimProduct)
                    .HasForeignKey(d => d.PimCustomerId)
                    .HasConstraintName("FK_PimProduct_PimCustomer");
            });

            modelBuilder.Entity<PimProductImage>(entity =>
            {
                entity.Property(e => e.B2bimageId).HasColumnName("B2BImageId");

                entity.Property(e => e.B2cimageId).HasColumnName("B2CImageId");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.HasOne(d => d.PimProduct)
                    .WithMany(p => p.PimProductImage)
                    .HasForeignKey(d => d.PimProductId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_PimProductImage_PimProduct");
            });

            modelBuilder.Entity<PimProductTag>(entity =>
            {
                entity.HasKey(e => new { e.PimProductId, e.PimTagId })
                    .HasName("PK_PimProductTag");

                entity.HasOne(d => d.PimProduct)
                    .WithMany(p => p.PimProductTag)
                    .HasForeignKey(d => d.PimProductId)
                    .HasConstraintName("FK_PimProductTag_PimProduct");

                entity.HasOne(d => d.PimTag)
                    .WithMany(p => p.PimProductTag)
                    .HasForeignKey(d => d.PimTagId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_PimProductTag_PimTag");
            });

            modelBuilder.Entity<PimProductType>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("IX_PimProductType_Name")
                    .IsUnique();

                entity.Property(e => e.DealerCyes).HasColumnName("DealerCYes");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<PimProductVariant>(entity =>
            {
                entity.Property(e => e.B2bvariantId).HasColumnName("B2BVariantId");

                entity.Property(e => e.B2cvariantId).HasColumnName("B2CVariantId");

                entity.Property(e => e.CommissionDealerA).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CommissionDealerB).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CommissionDealerC).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CostOfGoods).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CostOfGoodsFc).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.DealerASalePrice)
                    .HasColumnName("DealerASalePrice")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.DealerASalePriceWithGst)
                    .HasColumnName("DealerASalePriceWithGST")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.DealerBSalePrice)
                    .HasColumnName("DealerBSalePrice")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.DealerBSalePriceWithGst)
                    .HasColumnName("DealerBSalePriceWithGST")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.DealerCSalePrice)
                    .HasColumnName("DealerCSalePrice")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.DealerCSalePriceWithGst)
                    .HasColumnName("DealerCSalePriceWithGST")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.EscDollarsCustomer).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.EscDollarsDealerA).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.EscDollarsDealerB).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.EscDollarsDealerC).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.EscapeSrp).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.EscapeSrpDisc).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.EscapeSrpDiscWithGst)
                    .HasColumnName("EscapeSrpDiscWithGST")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.EscapeSrpWithGst)
                    .HasColumnName("EscapeSrpWithGST")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.ForeignCurrencyUnits).HasMaxLength(10);

                entity.Property(e => e.LandedCost).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.Msrp).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.MsrpFc).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.Option1).HasMaxLength(400);

                entity.Property(e => e.Option2).HasMaxLength(400);

                entity.Property(e => e.Option3).HasMaxLength(400);

                entity.Property(e => e.RetailEscDollars).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.RetailEscDollarsWithDisc).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.SaleOnB2b).HasColumnName("SaleOnB2B");

                entity.Property(e => e.ShippingAndDuties).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.ShopifyFulfillmentService).HasMaxLength(100);

                entity.Property(e => e.ShopifyInventoryManagement).HasMaxLength(100);

                entity.Property(e => e.ShopifyInventoryPolicy).HasMaxLength(100);

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.Weight).HasColumnType("decimal(14, 4)");

                entity.Property(e => e.WeightUnit)
                    .HasColumnName("WeightUnit ")
                    .HasMaxLength(10);

                entity.Property(e => e.XrateBuying)
                    .HasColumnName("XRateBuying")
                    .HasColumnType("decimal(14, 4)");

                entity.Property(e => e.XrateSelling)
                    .HasColumnName("XRateSelling")
                    .HasColumnType("decimal(14, 4)");

                entity.HasOne(d => d.PimBrandColour)
                    .WithMany(p => p.PimProductVariant)
                    .HasForeignKey(d => d.PimBrandColourId)
                    .HasConstraintName("FK_PimProductVariant_PimBrandColour");

                entity.HasOne(d => d.PimProduct)
                    .WithMany(p => p.PimProductVariant)
                    .HasForeignKey(d => d.PimProductId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_PimProductVariant_PimProduct");

                entity.HasOne(d => d.ProductImage)
                    .WithMany(p => p.PimProductVariant)
                    .HasForeignKey(d => d.ProductImageId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK_PimProductVariant_PimProductImage");
            });

            modelBuilder.Entity<PimSystemColour>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Colour).IsRequired();
            });

            modelBuilder.Entity<PimTag>(entity =>
            {
                entity.Property(e => e.Type).HasMaxLength(400);

            });


            modelBuilder.Entity<PimProductNote>(entity =>
            {
                
                entity.Property(e => e.PimProductId).IsRequired();

             //   entity.HasOne(d => d.PimProduct)
              //   .WithOne(p => p.PimProductNote)
             //    .HasForeignKey(x => x.PimProductId)
             //    .HasConstraintName("FK_PimProductNote_PimProduct");

            });



            /* the following section enables us to plularise the variables in the DBSet.
             * Entity framework otherwise assumes variable names as the table names 
             * */
            modelBuilder.Entity<PimBrand>().ToTable("PimBrand");
            modelBuilder.Entity<PimBrandColour>().ToTable("PimBrandColour");
            modelBuilder.Entity<PimCustomer>().ToTable("PimCustomer");
            modelBuilder.Entity<PimProduct>().ToTable("PimProduct");
            modelBuilder.Entity<PimProductImage>().ToTable("PimProductImage");
            modelBuilder.Entity<PimProductTag>().ToTable("PimProductTag");
            modelBuilder.Entity<PimProductType>().ToTable("PimProductType");
            modelBuilder.Entity<PimProductVariant>().ToTable("PimProductVariant");
            modelBuilder.Entity<PimSystemColour>().ToTable("PimSystemColour");
            modelBuilder.Entity<PimTag>().ToTable("PimTag");
            modelBuilder.Entity<PimProductNote>().ToTable("PimProductNote");


        }
    }
}
