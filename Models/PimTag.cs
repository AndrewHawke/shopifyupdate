﻿using System;
using System.Collections.Generic;

namespace ShopifyUpDATE.Models
{
    public partial class PimTag
    {
        public PimTag()
        {
            PimProductTag = new HashSet<PimProductTag>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }

        public virtual ICollection<PimProductTag> PimProductTag { get; set; }
    }
}
