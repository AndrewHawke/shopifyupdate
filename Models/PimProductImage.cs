﻿using System;
using System.Collections.Generic;

namespace ShopifyUpDATE.Models
{
    public partial class PimProductImage
    {
        public PimProductImage()
        {
            PimProductVariant = new HashSet<PimProductVariant>();
        }

        public int Id { get; set; }
        public int PimProductId { get; set; }
        public int? Position { get; set; }
        public string PimSrc { get; set; }
        public string ShopifySrc { get; set; }
        public long? B2cimageId { get; set; }
        public long? B2bimageId { get; set; }
        public long? WarehouseImageId { get; set; }
        public bool? IsDirty { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public virtual ICollection<PimProductVariant> PimProductVariant { get; set; }
        public virtual PimProduct PimProduct { get; set; }
    }
}
