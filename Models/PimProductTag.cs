﻿using System;
using System.Collections.Generic;

namespace ShopifyUpDATE.Models
{
    public partial class PimProductTag
    {
        public int PimProductId { get; set; }
        public int PimTagId { get; set; }

        public virtual PimProduct PimProduct { get; set; }
        public virtual PimTag PimTag { get; set; }
    }
}
