﻿using System;
using System.Collections.Generic;

namespace ShopifyUpDATE.Models
{
    public partial class PimCustomer
    {
        public PimCustomer()
        {
            PimProduct = new HashSet<PimProduct>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string SkuAbbreviation { get; set; }
        public string Domain { get; set; }
        public string CustomerWebsiteUrl { get; set; }
        public string BusinessWebsiteUrl { get; set; }
        public string WarehouseUrl { get; set; }
        public string DatabaseName { get; set; }
        public string Currency { get; set; }

        public virtual ICollection<PimProduct> PimProduct { get; set; }
    }
}
