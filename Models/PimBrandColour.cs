﻿using System;
using System.Collections.Generic;

namespace ShopifyUpDATE.Models
{
    public partial class PimBrandColour
    {
        public PimBrandColour()
        {
            PimProductVariant = new HashSet<PimProductVariant>();
        }

        public int Id { get; set; }
        public int PimBrandId { get; set; }
        public string BrandColour { get; set; }
        public string SystemColoursCsv { get; set; }

        public virtual ICollection<PimProductVariant> PimProductVariant { get; set; }
        public virtual PimBrand PimBrand { get; set; }
    }
}
