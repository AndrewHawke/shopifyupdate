﻿using System;
using System.Collections.Generic;

namespace ShopifyUpDATE.Models
{
    public partial class PimProduct
    {
        public PimProduct()
        {
            PimProductImage = new HashSet<PimProductImage>();
            PimProductTag = new HashSet<PimProductTag>();
            PimProductVariant = new HashSet<PimProductVariant>();


            //PimProductNote = new HashSet<PimProductNote>();
        }

        public int Id { get; set; }
        public int PimCustomerId { get; set; }
        public string Title { get; set; }
        public string Brand { get; set; }
        public int? ModelYear { get; set; }
        public string Description { get; set; }
        public string ProductType { get; set; }
        public string Options { get; set; }

      
        public string OptionsOrder { get; set; }
        public string DealerDiscipline { get; set; }
        public string Handle { get; set; }
        public DateTime? PublishedAt { get; set; }
        public bool? VisibleOnline { get; set; }
        public bool? VisiblePos { get; set; }
        public bool? IsDirty { get; set; }
        public string WarehouseStatus { get; set; }
        public long? B2cproductId { get; set; }
        public long? B2bproductId { get; set; }
        public long? WarehouseProductId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? B2bupdatedAt { get; set; }
        public DateTime? B2cupdatedAt { get; set; }
        public DateTime? WarehouseUpdatedAt { get; set; }

        public bool? IsDeactivated { get; set; }
        public bool? CheckPrice { get; set; }

        public bool? IsDistributor { get; set; }

        public virtual ICollection<PimProductImage> PimProductImage { get; set; }
        public virtual ICollection<PimProductTag> PimProductTag { get; set; }
        public virtual ICollection<PimProductVariant> PimProductVariant { get; set; }
        //public virtual ICollection<PimProductNote> PimProductNote { get; set; }

        public virtual PimProductNote PimProductNote { get; set; }
        public virtual PimCustomer PimCustomer { get; set; }
    }
}
