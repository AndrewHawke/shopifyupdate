﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopifyUpDATE.Models
{ 
    public partial class PimProductNote
    {

        public int Id { get; set; }
        public int PimProductId { get; set; }
        public string ProductNote { get; set; }
       
        public virtual PimProduct PimProduct { get; set; }
    }
}
