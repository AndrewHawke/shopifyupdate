﻿using System;
using System.Collections.Generic;

namespace ShopifyUpDATE.Models
{
    public partial class PimBrand
    {
        public PimBrand()
        {
            PimBrandColour = new HashSet<PimBrandColour>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<PimBrandColour> PimBrandColour { get; set; }
    }
}
